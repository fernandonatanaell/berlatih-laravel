@extends('adminLTE.master')

@section('navbar-item')
    <li class="nav-item d-none d-sm-inline-block"> 
        <a href="#" class="nav-link">SHOW</a>
    </li>
@endsection

@section('content')
    <div class="card text-center ">
        <div class="card-header bg-info">
            Cast Detail ID : {{ $person->id }}
        </div>
        <div class="card-body">
            <p class="h4 font-weight-bold">{{ $person->nama }} - {{ $person->umur }} tahun</p>
            <p class="card-text">{{ $person->bio }}</p>
            <a href="{{ route("cast.index") }}" class="btn btn-info">Back to the list</a>
        </div>
    </div>
@endsection
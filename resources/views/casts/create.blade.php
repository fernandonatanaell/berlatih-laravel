@extends('adminLTE.master')

@section('navbar-item')
    <li class="nav-item d-none d-sm-inline-block"> 
        <a href="#" class="nav-link">CREATE</a>
    </li>
@endsection

@section('content')
    <div class="card card-primary mr-5 ml-5">
        <div class="card-header">
            <h3 class="card-title">Create Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route("cast.store") }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}"
                    placeholder="Masukkan nama Anda">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}"
                    placeholder="Masukkan umur Anda">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label> <br>
                    <textarea name="bio" cols="100" rows="5" id="bio" value="{{ old('bio', '') }}"
                    placeholder="Enter bio here..."></textarea>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
@extends('adminLTE.master')


@section('navbar-item')
    <li class="nav-item d-none d-sm-inline-block"> 
        <a href="#" class="nav-link">LIST</a>
    </li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <li class="nav-item d-none d-sm-inline-block">
                <h3 class="text-danger">List of Cast</h3>
            </li>
            <li class="nav-item d-none d-sm-inline-block" style="float:right;">
                <a class="btn btn-primary" href="{{ route("cast.create") }}">Create</a>
            </li>
        </div>
        <div class="card-body p-0">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="col-1">#</th>
                    <th class="col-3">Nama</th>
                    <th class="col-2">Umur</th>
                    <th class="col-4">Bio</th>
                    <th class="col-2">Action</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $key => $cast)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $cast->nama }}</td>
                            <td>{{ $cast->umur }}</td>
                            <td>{{ $cast->bio }}</td>
                            <td class="d-flex flex-row border-0">
                                <a href="{{ route("cast.show", ["cast" => $cast->id]) }}" class="btn btn-info p-2 mr-2">Show</a>
                                <a href="{{ route("cast.edit", ["cast" => $cast->id]) }}" class="btn btn-secondary p-2 mr-2">Edit</a>
                                <form action="{{ route("cast.destroy", ["cast" => $cast->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger p-2" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No Casts</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('scripts')
    @if (session('success'))
            <script>
                Swal.fire({
                    title: "Berhasil!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    confirmButtonText: "Cool",
                });
            </script>
    @endif
@endpush
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    //
    public function index(){
        // $casts = DB::table('cast')->get();

        $casts = Cast::all();

        return view('casts.index', compact('casts'));
    }

    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        $query = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Berhasil ditambahkan!');
    }

    public function show($id){
        // $person = DB::table('cast')->where('id', $id)->first();
        $person = Cast::where('id', $id)->first();

        return view('casts.show', compact('person'));
    }

    public function edit($id){
        // $person = DB::table('cast')->where('id', $id)->first();
        $person = Cast::where('id', $id)->first();

        return view('casts.edit', compact('person'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         "nama" => $request["nama"],
        //         "umur" => $request["umur"],
        //         "bio" => $request["bio"]
        //     ]);

        $query = Cast::where('id', $id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Berhasil diupdate!');
    }

    public function destroy($id){
        // $query = DB::table('cast')->where('id', $id)->delete();

        Cast::destroy($id);

        return redirect('/cast')->with('success', 'Berhasil dihapus!');;
    }
}
